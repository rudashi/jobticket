<?php

namespace JobTicket\Tests;

use Rudashi\JobTicket\Model\JobBindery;
use Rudashi\JobTicket\Model\JobPrint;
use Rudashi\JobTicket\Traits\ProfisQueries;
use Rudashi\Optima\Classes\Collection;
use Rudashi\Profis\Repositories\Contracts\OrderRepositoryInterface;
use Tests\CreatesApplication;
use Illuminate\Foundation\Testing\TestCase;

class JobsCalculatePrintingSetsAndQuantityTest extends TestCase
{
    use CreatesApplication;

    private JobTicketProfisQueries $repository;
    private array $binding = [];

    public function setUp(): void
    {
        parent::setUp();
        $this->binding = [
            'personalization' => new Collection(),
            'book_line' => new Collection(),
            'binding_type' => new Collection(),
        ];
        $this->repository = app(JobTicketProfisQueries::class);
    }

    private function getPrintSets(Collection $order, int $service, bool $bookLine = false): ?int
    {
        $job = $order->firstWhere('service_id', $service);

        if ($bookLine) {
            $this->binding['book_line']->push($job->task_id);
        }
        return (new JobPrint($job, new Collection(), $this->binding))->sets;
    }

    private function getBindQty(Collection $order, int $service): int
    {
        return  (new JobBindery($order->firstWhere('service_id', $service)))->quantity;
    }

    public function test_order_12512(): void
    {
        $order = $this->repository->jobs(167951);

        self::assertSame(1028, $this->getPrintSets($order, 1350015)); //Okładka
        self::assertSame(508, $this->getPrintSets($order,1350011)); //Wnętrze 1+1 szyte nićmi

        self::assertSame(1026, $this->getBindQty($order, 1350016)); //Laminowanie matowe                  1026
        self::assertSame(1000, $this->getBindQty($order, 1350018)); //Oprawa miękka HOTMELT               1011
        self::assertSame(1011, $this->getBindQty($order, 1350012)); //Szycie nićmi standard               1011
        self::assertSame(1011, $this->getBindQty($order, 1350017)); //Zaklejanie skrzydełek okładki       1011
    }

    public function test_order_12595(): void
    {
        $order = $this->repository->jobs(168035);

        self::assertSame(122, $this->getPrintSets($order,1351068)); //Drukowanie okladki/oklejki/obwoluty R9110 4+0 do 487
        self::assertSame(108, $this->getPrintSets($order,1351063)); //Drukowanie wnętrz R9110 4+4 do 360
        self::assertSame(100, $this->getPrintSets($order,1351060)); //Drukowanie wnętrz VarioPrint TP B4 1+1

        self::assertSame(105, $this->getBindQty($order, 1351070)); //Folia na gorąco KURTZ
        self::assertSame(120, $this->getBindQty($order, 1351069)); //Laminowanie matowe
        self::assertSame(100, $this->getBindQty($order, 1351071)); //Oprawa miękka HOTMELT
        self::assertSame(105, $this->getBindQty($order, 1351061)); //Szycie nićmi standard
        self::assertSame(105, $this->getBindQty($order, 1351064)); //Szycie nićmi standard
    }

    public function test_order_12335(): void
    {
        $order = $this->repository->jobs(167773);

        self::assertSame(59, $this->getPrintSets($order,1347797)); //Drukowanie okladki/oklejki/obwoluty KM1 4+0 B2
        self::assertSame(25, $this->getPrintSets($order,1347788)); //Drukowanie wnętrz VarioPrint TP A3 1+1
        self::assertNull($this->getPrintSets($order,1347794)); //Wyklejki 0+0 0                                                 (profis 100 - ma być NIC)
        self::assertNull($this->getPrintSets($order,1347803)); //Wykonanie tekturek do okładki opr.twardej                      (profis 37  - ma być NIC)

        self::assertSame(100, $this->getBindQty($order,1347789)); //Gumka zamykająca do 220mm
        self::assertSame(103, $this->getBindQty($order,1347801)); //Kompletowanie bloku
        self::assertSame(57, $this->getBindQty($order,1347798)); //Laminowanie matowe anti-scratch
        self::assertSame(115, $this->getBindQty($order,1347799)); //Montaż okładki dla oprawy spiralowanej
        self::assertSame(100, $this->getBindQty($order,1348465)); //Po krótkim boku - miejsce założenia spirali
        self::assertSame(100, $this->getBindQty($order,1348464)); //PVC - CZARNA
        self::assertSame(105, $this->getBindQty($order,1347800)); //Wyklejanie okladki oprawy spiralowanej twardej
        self::assertSame(100, $this->getBindQty($order,1347795)); //Wyklejki zadrukowane
        self::assertSame(100, $this->getBindQty($order,1347802)); //Oprawa spirala Wire-o 28mm do 240 mm
    }

    public function test_order_11925(): void
    {
        $order = $this->repository->jobs(167360);

        self::assertSame(511, $this->getPrintSets($order,1342505)); //Drukowanie okladki/oklejki/obwoluty KM1 4+4 B2
        self::assertSame(2029, $this->getPrintSets($order,1342500, true)); //Drukowanie wnętrz 1+1 CS3700 Próg 2

        self::assertSame(45504, $this->getBindQty($order, 1342501)); //Cięcie wstęgi - Bookline                                 (sam liczy 215481)
        self::assertSame(509, $this->getBindQty($order, 1342506)); //Laminowanie matowe
        self::assertSame(2020, $this->getBindQty($order, 1342507)); //Oprawa miękka HOTMELT
        self::assertSame(2020, $this->getBindQty($order, 1342502)); //Wyrywanie wakatów
    }

    public function test_order_12723(): void
    {
        $order = $this->repository->jobs(168170);

        self::assertSame(207, $this->getPrintSets($order, 1352876)); //Drukowanie okladki/oklejki/obwoluty R9110 4+0 do 487
        self::assertSame(208, $this->getPrintSets($order,1352873)); //Drukowanie wnętrz R9110 4+4 do 360

        self::assertSame(205, $this->getBindQty($order, 1352877)); //Laminowanie matowe
        self::assertSame(200, $this->getBindQty($order, 1352878)); //Oprawa miękka HOTMELT
        self::assertSame(205, $this->getBindQty($order, 1352874)); //Szycie nićmi standard
    }

    public function test_order_12337(): void
    {
        $order = $this->repository->jobs(167775);

        self::assertSame(37, $this->getPrintSets($order, 1347833)); //Drukowanie okladki/oklejki/obwoluty KM1 4+0 B2
        self::assertSame(17, $this->getPrintSets($order,1347830)); //Drukowanie wnętrz na KM1 4+0 B2
        self::assertSame(17, $this->getPrintSets($order,1347828)); //Drukowanie wnętrz na KM1 4+4 B2

        self::assertSame(35, $this->getBindQty($order, 1347834)); //Laminowanie matowe
        self::assertSame(100, $this->getBindQty($order, 1347835)); //Oprawa spirala Wire-o 14mm do 210 mm
        self::assertSame(100, $this->getBindQty($order, 1347848)); //Po długim boku - miejsce założenia spirali
        self::assertSame(100, $this->getBindQty($order, 1347838)); //PVC - PRZEZROCZYSTA
    }

    public function test_order_11278_old(): void
    {
        $order = $this->repository->jobs(166711);

        self::assertSame(57, $this->getPrintSets($order,1333778)); //Druk okładki 4+0 KM1
        self::assertSame(100, $this->getPrintSets($order,1333775, true)); //Drukowanie 1+1 CS3700 Próg 2

        self::assertSame(102, $this->getBindQty($order, 1333779)); //Foliowanie mat
        self::assertSame(100, $this->getBindQty($order, 1333776)); //Obróbka Hunkeler Bookline
        self::assertSame(100, $this->getBindQty($order, 1333780)); //Oprawa klejona ze skrzydełkami
    }

}

class JobTicketProfisQueries {
    use ProfisQueries;

    public OrderRepositoryInterface $profis;

    public function __construct(OrderRepositoryInterface $profis)
    {
        $this->profis = $profis;
    }

    public function jobs($order_id): Collection
    {
        return $this->fetchJobs($order_id)->map(function($job) {
            $job->materials = new Collection();
            return $job;
        });
    }

}
