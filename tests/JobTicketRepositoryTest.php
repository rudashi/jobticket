<?php

namespace JobTicket\Tests;

use Rudashi\JobTicket\Enums\OrderPhraseType;
use Rudashi\JobTicket\Repositories\JobTicketRepository;
use Rudashi\Profis\Classes\Filters;
use Rudashi\Profis\Enums\Status;
use Rudashi\Profis\Repositories\OrderRepository;
use Tests\CreatesApplication;
use Illuminate\Foundation\Testing\TestCase;

class JobTicketRepositoryTest extends TestCase
{

    private JobTicketRepository $repository;
    private OrderRepository $profis;

    use CreatesApplication;

    public function setUp(): void
    {
        parent::setUp();

        $this->repository = app(JobTicketRepository::class);
        $this->profis = app(OrderRepository::class);
    }

    public function testFindOrdersByPhrase() : void
    {
        $orders = $this->repository->findOrdersByPhrase(
            OrderPhraseType::NUMBER,
            '00666/2019',
            new Filters([
                'archived' => 1,
                'not_status' => Status::Canceled
            ])
        );

        self::assertCount(1, $orders);
    }

    public function testGetOrderMaterials(): void
    {
//        $id = 155334;
//        $jobs = $this->profis->getTasks($id);
//        $materials = $this->profis->fetchMaterials($jobs->pluck('task_id'));
        self::markTestSkipped();
    }
}
