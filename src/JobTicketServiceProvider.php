<?php

namespace Rudashi\JobTicket;

use Illuminate\Support\ServiceProvider;
use Totem\SamCore\App\Traits\TraitServiceProvider;

class JobTicketServiceProvider extends ServiceProvider
{

    use TraitServiceProvider;

    public function getNamespace(): string
    {
        return 'job-ticket';
    }

    public function boot(): void
    {
        $this->loadAndPublish(__DIR__ . '/lang', __DIR__ . '/database/migrations', __DIR__ . '/views');

        $this->publishes([
            __DIR__ . '/config/config.php' => config_path($this->getNamespace() . '.php'),
        ], $this->getNamespace().'-config');

        $this->loadRoutesFrom(__DIR__ . '/routes/api.php');
    }

    public function register(): void
    {
        $this->mergeConfigFrom(__DIR__ . '/config/config.php', $this->getNamespace());

        $this->app
            ->when(\Rudashi\JobTicket\Controllers\FileController::class)
            ->needs(\Rudashi\JobTicket\Repositories\Contracts\JobTicketRepositoryInterface::class)
            ->give(\Rudashi\JobTicket\Repositories\JobTicketRepository::class);

        $this->app
            ->when(\Rudashi\JobTicket\Controllers\JobTicketController::class)
            ->needs(\Rudashi\JobTicket\Repositories\Contracts\JobTicketRepositoryInterface::class)
            ->give(\Rudashi\JobTicket\Repositories\JobTicketRepository::class);

        $this->app
            ->when(\Rudashi\JobTicket\Repositories\JobTicketRepository::class)
            ->needs(\Rudashi\Profis\Repositories\Contracts\OrderRepositoryInterface::class)
            ->give(\Rudashi\Profis\Repositories\OrderRepository::class);

        $this->app
            ->when(\Rudashi\JobTicket\Repositories\JobTicketRepository::class)
            ->needs(\Rudashi\Optima\Repositories\Contracts\CustomerInterface::class)
            ->give(\Rudashi\Optima\Repositories\CustomerRepository::class);
    }

}
