<?php

return [

    /**
     * CONSTANT keyword used in order name to determine is complaint
     */
    'COMPLAINT_KEYWORD' => 'REKLAMACJA',

    /**
     * CONSTANT keyword used to exclude supernumerary information about edition
     */
    'SUPERNUMERARY_EDITION_KEYWORD' => 'Książki ponadnakładowe:',

    /**
     * CONSTANT defines default flap thickness if not provided
     */
    'DEFAULT_FLAP_THICKNESS' => 1,

    /**
     * CONSTANT defines default ID for sewing selector (PBS.Zadania -> id_skladki)
     */
    'SELECTOR_SEWING' => [102, 104, 108, 105],

    'PROCESS' => [
        /**
         * CONSTANT defines ID for offset printing (PBS.Obrobki)
         */
        'OFFSET_ID' => 23,

        /**
         * CONSTANT defines ID of personalization process (PBS.Obrobki)
         */
        'PERSONALIZATION_ID' => 94,

        /**
         * CONSTANT defines ID for endpaper process (PBS.Obrobki)
         */
        'ENDPAPER_ID' => 41,

        /**
         * CONSTANT defines ID of reprint process (PBS.Obrobki)
         */
//        'REPRINT_ID' => 48,

        /**
         * CONSTANT defines ID of graphics file fix process (PBS.Obrobki)
         */
//        'FIX_FILE_GRAPHICS_ID' => 89,

        /**
         * CONSTANT defines ID of prepress file fix process (PBS.Obrobki)
         */
//        'FIX_FILE_PREPRESS_ID' => 92,

        /**
         * CONSTANT defines ID of soft-proof process (PBS.Obrobki)
         */
        'PROOF_ID' => 33,

        /**
         * CONSTANT defines ID for book blocks inline process (PBS.Obrobki)
         */
        'BOOK_LINE_ID' => [68, 127],

        /**
         * CONSTANT defines ID for foiling paper process (PBS.Obrobki)
         */
        'FOILING_ID' => 6,

        /**
         * CONSTANT defines ID for refresh line print process (PBS.Obrobki)
         */
        'REFRESH_LINE_ID' => [
            88, //inactive
            160
        ],

        /**
         * CONSTANT defines ID for ribbon process (PBS.Obrobki)
         */
        'RIBBON_ID' => 32,

        /**
         * CONSTANT defines ID for spine type process (PBS.Obrobki)
         */
//        'SPINE_ID' => 30,

        /**
         * CONSTANT defines ID for cardboard process (PBS.Obrobki)
         */
        'CARDBOARD_ID' => 83,

        /**
         * CONSTANT defines ID for spiral process (PBS.Obrobki)
         */
        'SPIRAL_ID' => 136,

        /**
         * CONSTANT defines ID for spiral color process (PBS.Obrobki)
         */
        'SPIRAL_COLOR_ID' => 152,

        /**
         * CONSTANT defines ID for spiral position process (PBS.Obrobki)
         */
        'SPIRAL_POSITION_ID' => 153,

        /**
         * CONSTANT defines minimum order for shipping processes (PBS.Obrobki.kolejnosc_domyslna)
         */
        'ORDER_SHIPPING_LIMIT' => [
            1000,
            1999
        ],

    ],

    'SCHEMA' => [
        /**
         * CONSTANT defines ID for digital printing task schema (PBS.SzablonyZd)
         */
//        'DIGITAL_PRINT' => 66,

        /**
         * CONSTANT defines ID for hardcover wire-o cover task schema (PBS.SzablonyZd)
         */
        'HARDCOVER_WIRE_O_COVER_PRINT' => 165,

        /**
         * CONSTANT defines ID for unprinted endpaper task schema (PBS.SzablonyZd)
         */
        'ENDPAPER' => [
            157, 166, 158, 167, 159, 163
        ],
    ],

    'ARTICLE' => [
        /**
         * CONSTANT defines spine type flat in optima article database (CDN.Towary)
         */
//        'SPINE_FLAT_ID' => 1325,

        /**
         * CONSTANT defines spine type round in optima article database (CDN.Towary)
         */
//        'SPINE_ROUND_ID' => 1326,

        /**
         * CONSTANT defines spiral position in optima article database (CDN.Towary)
         */
        'SPIRAL_SHORT_ID' => 14783,

        /**
         * CONSTANT defines spiral position in optima article database (CDN.Towary)
         */
        'SPIRAL_LONG_ID' => 14784,

        /**
         * CONSTANT defines bookline in optima article database (CDN.Towary)
         */
        'BOOK_LINE_ID' => 14630,

        /**
         * CONSTANT defines cutter in optima article database (CDN.Towary)
         */
        'CUTTER_ID' => 14631,

        /**
         * CONSTANT defines UNIVERSE in optima artivle database (CDN.Towary)
         */
        'UNIVERSE_WEB_ID' => [
            15139,
            15140,
            15141,
            15142,
        ],
    ],

    'DICTIONARY' => [

        /**
         * CONSTANT defines spine type flat in optima article database (PBS.SlowElem)
         */
        'SPINE_FLAT_ID' => 112,

        /**
         * CONSTANT defines spine type round in optima article database (PBS.SlowElem)
         */
        'SPINE_ROUND_ID' => 113,
    ]

];
