<?php

namespace Rudashi\JobTicket\Traits;

use Illuminate\Database\Query\Expression;
use InvalidArgumentException;
use Rudashi\JobTicket\Model\Contracts\JobInterface;
use Rudashi\JobTicket\Model\Job;
use Rudashi\JobTicket\Model\JobExecution;
use Rudashi\JobTicket\Model\Order;
use Rudashi\Optima\Classes\Collection;
use Rudashi\Profis\Classes\Models\Contracts\ExecutionInterface;

trait ProfisQueries
{

    private function findOrder(int $order_id): Order
    {
        $data = $this->profis->newQuery()
            ->from('PBS.Zlecenia as order')
            ->select([
                'order.id_zlecenia as order_id',
                'order.nr_zlecenia as order_number',
                'order.termin_realizacji as order_deadline',
                'order.nazwa_zlecenia as order_name',
                'order.isbn as isbn',
                'order.naklad_gotowego as qty_total',
                'order.archiwum as archived',

                'order.data_zlecenia as order_date',
                'order.uwagi as notes',
                'order.uwagi2 as notes_internal',
                'order.zalaczniki as notes_after',
                'template.format_nazwy_pliku as binding_type',
                'template.opis as binding_description',

                'order.id_klienta as customer_id',

                'order.typ_isbn as selector_isbn_type',
                'order.id_powodu as selector_order_type',
                'order.id_podstatusu as selector_status_id',

                'order.masa_egz as mass_unit',
                'order.masa_calkowita as mass_total',

                'order.nr_umowy as flaps',

                'service.mail as customer_service_email',
                'service.na_uzytkownicy as customer_service_fullname',

                'manager.mail as customer_manager_email',
                'manager.na_uzytkownicy as customer_manager_fullname',
            ])
            ->leftJoin('PBS.ParZlecenia as template', 'template.id_parzlecenia', 'order.id_parzlecenia')
            ->leftJoin('PBS.defuzytkownicy as service', 'service.id_uzyt', 'order.id_uzyt')
            ->leftJoin('PBS.defuzytkownicy as manager', 'manager.id_uzyt', 'order.id_uzyt_wpis')
            ->where('order.id_zlecenia', $order_id)
            ->first()
        ;

        if ($data === null) {
            throw new InvalidArgumentException(__('Given Order Number :code is invalid or not in the MiS system.', ['code' => $order_id]), 404);
        }
        if ($data->selector_order_type === null) {
            throw new InvalidArgumentException(__('Given Order Number :code has invalid :attribute.', ['code' => $order_id, 'attribute' => 'Powód']), 422);
        }

        return new Order((array) $data);
    }

    private function fetchJobs(int $order_id): Collection
    {
        return $this->attachExecutionsToJobs(
            $this->profis->newQuery()
                ->from('PBS.Zadania as task')
                ->select([
                    'service.id_uslugi as service_id',
                    'task.id_zadania as task_id',
                    'service.id_obrobki as process_id',
                    'template.id_rodzaju_zadania as task_type_id',
                    'task.id_szablonu_zd as task_schema_id',

                    'task.nazwa_zadania as task_name',
                    'machine.nazwa_maszyny as machine_name',
                    'service.nazwa_uslugi as service_name',
                    'service.uwagi as service_note',

                    'goods.Twr_KatId as quality',

                    'task.kolory1 as sheet_verso',
                    'task.kolory2 as sheet_recto',

                    'process.typ_maszyny as machine_type_id',
                    'task.naklad_gotowego as order_quantity',
                    'service.zwyzka_zewn_etapu as quantity_encore',
                    'service.uzytki as pieces',
                    'task.uzytki_szerokosc as pieces_width',
                    'task.uzytki_wysokosc as pieces_height',
                    new Expression('(service.ilosc_ofertowa / service.dwustronny) + service.zwyzka_zewn_etapu as quantity_proposal'),

                    'task.id_skladki as sewing',
                    'process.wymiar_mnoznika as printing_roller',
                    'task.szerokosc_druku as printing_width',
                    'task.wysokosc_druku as printing_height',

                    'service.ilosc_ofertowa as quantity',
                    'service.ilosc as sheets',
                    'service.dwustronny as duplex',
                    'service.liczba_wykonana as quantity_done',

                    'task.strony_gotowe as order_pages',
                    'task.szerokosc_gotowego as order_width',
                    'task.wysokosc_gotowego as order_height',

                    'process.kolejnosc_domyslna as process_order',
                    'process.id_typu_zasobu as process_asset_id',
                    'service.wsad as batch',

                    'service.termin_wykonania as execution_date',
                    'selector.wartosc as service_status',

                    'service.id_towaru as article_id',
                    'service.id_dostawy as shipping_id',
                    'service_param.karty_max as step',
                    'spines.rodzaj_grzbietu as spine',
                ])
                ->leftJoin('PBS.Uslugi as service', 'task.id_zadania', 'service.id_zadania')
                ->leftJoin('PBS.Obrobki as process', 'process.id_obrobki', 'service.id_obrobki')
                ->leftJoin('PBS.selektory as selector', 'selector.id_sel', 'service.id_statusu_wykonania')
                ->leftJoin('PBS.Zlecenia as order', 'order.id_zlecenia', 'task.id_zlecenia')
                ->leftJoin('PBS.SzablonyZd as template', 'task.id_szablonu_zd', 'template.ID_szablonu_zd')
                ->leftJoin('PBS.Maszyny as machine', 'task.id_maszyny', 'machine.id_maszyny')
                ->leftJoin('CDN.Towary as goods', 'goods.Twr_TwrId', 'service.id_towaru')
                ->leftJoin('PBS.ParUslugi as service_param', 'service_param.id_towaru', 'goods.Twr_TwrId')
                ->leftJoin('PBS.Grzbiety as spines', 'spines.id_zadania', 'task.id_zadania')
                ->where('order.id_zlecenia', $order_id)
                ->get()
                ->mapInto(Job::class)
        );
    }

    private function attachExecutionsToJobs($jobs)
    {
        $executions = $this->profis->fetchExecutionsByService($jobs->pluck('service_id'));

        return $jobs->map(static function (JobInterface $job) use ($executions) {
            return $job->setExecutions(
                $executions->filter(static fn(ExecutionInterface $execution) => $job->service_id === $execution->service_id)
                    ->mapInto(JobExecution::class)
                    ->values()
            );
        });
    }
}
