<?php

namespace Rudashi\JobTicket\Services;

use BenSampo\Enum\Exceptions\InvalidEnumKeyException;
use JsonSerializable;
use Rudashi\Profis\Enums\BindType;
use Rudashi\BookGenerator\App\Enums\CoverType;
use Rudashi\JobTicket\Model\JobPrint;
use Rudashi\JobTicket\Model\JobTicket;
use Rudashi\Optima\Classes\Collection;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;

class Alerts implements Arrayable, Jsonable, JsonSerializable
{

    private array $messages = [];

    public function run(JobTicket $jobTicket): void
    {
//        $this->setRefreshLineAlert($jobTicket->jobs_print);
        $this->setMultipleRibbonAlert($jobTicket->jobs_bindery);
        $this->setHasPersonalizationAlert($jobTicket->personalization);
        $this->setIsComplaintAlert($jobTicket->order_name);
        $this->setHasFlaps($jobTicket->covers, $jobTicket->flaps);
        $this->setWireOBookLineAlert($jobTicket->binding_type, $jobTicket->jobs_bindery);
    }

    public function toJson($options = 0)
    {
        return json_encode($this->jsonSerialize(), $options);
    }

    public function jsonSerialize(): array
    {
        return $this->toArray();
    }

    public function toArray(): array
    {
        return $this->messages;
    }

    public function __toString(): string
    {
        return $this->toJson();
    }

//    private function setRefreshLineAlert(Collection $jobs): void
//    {
//        $diff = false;
//        if ($jobs->contains('process_id', config('job-ticket.PROCESS.REFRESH_LINE_ID'))) {
//            $diff = $jobs->map(function(JobPrint $job) {
//                return $job->materials && $job->materials->roll_width ? $job->materials->name : null;
//            })->filter()->unique()->count() !== 1;
//        }
//        $this->add('refresh_line', $diff);
//    }

    private function setMultipleRibbonAlert(Collection $jobs): void
    {
        $diff = $jobs->where('process_id', config('job-ticket.PROCESS.RIBBON_ID'))->pluck('id');

        if ($diff->count() > 1) {
            $this->add('ribbon', $diff);
        }
    }

    private function setHasPersonalizationAlert(Collection $jobs): void
    {
        $this->add('personalization', $jobs->isNotEmpty());
    }

    public function setIsComplaintAlert(string $order_name): void
    {
        $this->add('complaint', strpos($order_name, config('job-ticket.COMPLAINT_KEYWORD')) !== false);
    }

    private function add($key, $message): void
    {
        $this->messages[$key] = $message;
    }

    private function setHasFlaps(Collection $covers, $flaps): void
    {
        $diff = (bool) $flaps;

        if ($covers->has(CoverType::getKey(CoverType::JACKET))) {
            $diff = false;
        }

        $this->add('flaps', $diff);
    }

    private function setWireOBookLineAlert($binding_type, Collection $jobs_bindery): void
    {
        try {
            if ((BindType::WIRE_O === BindType::fromKey($binding_type)->value) && $jobs_bindery->contains('process_id', config('job-ticket.ARTICLE.BOOK_LINE_ID'))) {
                $this->add('wire_o_book_line', true);
            }
        } catch (InvalidEnumKeyException $e) {
            $this->add('binding', 'Cannot identify binding: '.$binding_type);
        }
    }

}
