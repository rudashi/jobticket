<?php

namespace Rudashi\JobTicket\Enums;

class OrderPhraseType
{

    public const NUMBER     = 1;
    public const CUSTOMER   = 2;
    public const TITLE      = 3;
    public const ALL        = 9;

}
