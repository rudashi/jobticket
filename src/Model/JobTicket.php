<?php

namespace Rudashi\JobTicket\Model;

use BenSampo\Enum\Exceptions\InvalidEnumKeyException;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use Rudashi\JobTicket\Services\Alerts;
use Rudashi\Optima\Classes\Collection as OptimaCollection;
use Rudashi\Profis\Classes\Models\Contracts\CustomerInterface;
use Rudashi\Profis\Classes\Models\Contracts\SelectorInterface;
use Rudashi\Profis\Enums\BindType;

class JobTicket
{

    public int $order_id;
    public string $order_number;
    public string $status;
    public string $order_type;
    public string $binding_type;
    public ?string $binding_description = null;
    public ?object $flaps = null;

    public Carbon $order_date;
    public Carbon $order_deadline;

    public CustomerInterface $customer;
    public string $customer_manager; //opiekun
    public string $order_name;
    public string $customer_service; //obsługujący
    public object $publication_number;
    public ?JobExecution $technologist = null;

    public ?object $format = null;
    public int $pages;
    public int $qty_total;

    public float $mass_unit;
    public float $mass_total;

    public ?string $notes = null;
    public ?string $notes_supernumerary = null;
    public ?float $spine = null;

    public Job $jobs_main;
    public OptimaCollection $jobs_prepress;
    public OptimaCollection $jobs_print;
    public OptimaCollection $jobs_bindery;
    public OptimaCollection $jobs_shipping;
    public OptimaCollection $shipping_delivery;
    public OptimaCollection $technology;
    public OptimaCollection $proof;
    public OptimaCollection $personalization;
    public OptimaCollection $covers;

    public Alerts $alerts;

    public function setOrder(Order $order): JobTicket
    {
        $this->order_id             = $order->order_id;
        $this->order_number         = $order->order_number;
        $this->order_deadline       = $order->order_deadline;
        $this->order_date           = $order->order_date;
        $this->order_name           = $order->order_name;
        $this->qty_total            = $order->qty_total;
        $this->binding_type         = BindType::getBinding($order->binding_type);
        $this->binding_description  = $order->binding_description;
        $this->flaps                = $order->flaps;
        $this->customer_manager     = $order->customer_manager_fullname;
        $this->customer_service     = $order->customer_service_fullname;
        $this->mass_unit            = $order->mass_unit;
        $this->mass_total           = $order->mass_total;
        $this->notes                = preg_replace('/\n/', '', $order->notes);
        $this->notes_supernumerary  = $order->notes_after ? $this->extractSupernumeraryNote($order->notes_after) : null;
        $this->alerts               = new Alerts();

        $this->setCustomSpine($order->notes_internal);

        return $this;
    }

    public function setCustomer(CustomerInterface $customer): JobTicket
    {
        $this->customer = $customer;

        return $this;
    }

    public function setPublication(SelectorInterface $selector, ?string $number): JobTicket
    {
        $this->publication_number = (object) [
            'type' => $selector->value,
            'number' => $number
        ];

        return $this;
    }

    public function setOrderStatus(SelectorInterface $selector): JobTicket
    {
        $this->status = $selector->value;

        return $this;
    }

    public function setOrderType(SelectorInterface $selector): JobTicket
    {
        $this->order_type = $selector->value;

        return $this;
    }

    public function setFormat(Job $job = null): JobTicket
    {
        $this->format = $job ? (object) [
            'width' => $job->order_width,
            'height' => $job->order_height,
        ] : null;

        return $this;
    }

    public function setPages(OptimaCollection $jobs_print, OptimaCollection $jobs_for_inner): JobTicket
    {
        if ($jobs_for_inner->isEmpty()) {
            $this->pages = $jobs_print->sum('order_pages');
        } else {
            $this->pages = $jobs_for_inner->sum(function(Job $job) {
                return $job->duplex ? $job->order_pages : $job->order_pages * 2;
            });
        }

        return $this;
    }

    public function setJobMain(OptimaCollection $task): JobTicket
    {
        $this->jobs_main = $task->first();

        return $this;
    }

    public function setJobPrepress(OptimaCollection $tasks): JobTicket
    {
        $this->jobs_prepress = $tasks->mapInto(JobPrepress::class)->values();

        return $this;
    }

    public function setPersonalization(OptimaCollection $tasks): JobTicket
    {
        $this->personalization = $tasks
            ->where('process_id', config('job-ticket.PROCESS.PERSONALIZATION_ID'))
            ->values();

        return $this;
    }

    public function setJobsPrint(OptimaCollection $tasks, OptimaCollection $colors): JobTicket
    {
        $options = [
            'binding_type' => $this->binding_type,
            'personalization' => $this->personalization->pluck('task_id'),
            'jobs_main' => $this->jobs_main,
            'book_line' => $this->jobs_bindery->filter(static fn(JobBindery $job) => $job->article_id === config('job-ticket.ARTICLE.BOOK_LINE_ID'))->pluck('task_id'),
            'cutter' => $this->jobs_bindery->filter(static fn(JobBindery $job) => $job->article_id === config('job-ticket.ARTICLE.CUTTER_ID'))->pluck('task_id'),
            'refresh_line' => $this->jobs_bindery->whereIn('process_id', config('job-ticket.PROCESS.REFRESH_LINE_ID'))->pluck('task_id'),
            'universe_web' => $this->jobs_bindery->whereIn('article_id', config('job-ticket.ARTICLE.UNIVERSE_WEB_ID'))->pluck('task_id'),
        ];

        $this->jobs_print = $tasks->sortBy(static function(Job $job) {
            return [$job->task_id, $job->process_order];
        })->map(static function(Job $job) use ($colors, $options) {
            return new JobPrint($job, $colors, $options);
        })->filter(static function(JobPrint $job) {
            return $job->materials->resource_id !== config('profis.MATERIAL.NOTHING');
        })->values();

        return $this;
    }

    public function setJobsBindery(OptimaCollection $tasks): JobTicket
    {
        $options = [

        ];

        $this->jobs_bindery = $tasks->sortBy(static function(Job $job) {
            return [$job->task_id, $job->process_order];
        })->map(static function(Job $job) use ($options) {
            return new JobBindery($job, $options);
        })->values();

        return $this;
    }

    public function setJobsShipping(OptimaCollection $tasks): JobTicket
    {
        $this->jobs_shipping = $tasks->mapInto(JobsShipping::class)->values();

        return $this;
    }

    public function setShippingDelivery(OptimaCollection $shipping): JobTicket
    {
        $this->shipping_delivery = $shipping;

        return $this;
    }

    public function setCustomSpine(?string $notes): JobTicket
    {
        if ($notes) {
            preg_match('/\[(.*)]/', $notes, $output);

            $this->spine = isset($output[1]) ? (float) str_replace(',', '.', $output[1]) : null;
        }

        return $this;
    }

    public function setCoversCalculation(OptimaCollection $covers, OptimaCollection $papers): JobTicket
    {
        if ($papers->isEmpty()) {
            $this->covers = new OptimaCollection();

            return $this;
        }

        $this->covers = $covers->mapWithKeys(function(Job $job) use ($covers, $papers) {
            $cover = (new CoverRequestParameters($job, $papers))
                ->setBindingType($this->binding_type)
                ->setWidth($this->format->width)
                ->setHeight($this->format->height)
                ->setCardboard($this->jobs_print->where('process_id', config('job-ticket.PROCESS.CARDBOARD_ID'))->first())
                ->setFlaps($covers, $this->flaps)
                ->setCustomSpine($this->spine);

            if (in_array($cover->bind_type, [BindType::WIRE_O, BindType::HARDCOVER_WIRE_O], true)) {
                $spiral_services = $this->jobs_bindery->whereIn('process_id', [
                    config('job-ticket.PROCESS.SPIRAL_ID'),
                    config('job-ticket.PROCESS.SPIRAL_COLOR_ID'),
                    config('job-ticket.PROCESS.SPIRAL_POSITION_ID'),
                ]);
                if ($spiral_services->count() === 3) {
                    $cover->setSpiral(
                        $this->jobs_bindery->firstWhere('process_id', config('job-ticket.PROCESS.SPIRAL_ID')),
                        $this->jobs_bindery->firstWhere('process_id', config('job-ticket.PROCESS.SPIRAL_COLOR_ID')),
                        $this->jobs_bindery->whereIn('article_id', [
                            config('job-ticket.ARTICLE.SPIRAL_SHORT_ID'),
                            config('job-ticket.ARTICLE.SPIRAL_LONG_ID'),
                        ])->first(),
                    );
                }
            }

            return [$cover->name => $cover];
        });


        return $this;
    }

    public function validateJobTicket(): JobTicket
    {
        $this->alerts->run($this);

        return $this;
    }

    public function getBinding(string $property): string
    {
        try {
            return BindType::fromKey($this->binding_type)->{$property};
        } catch (InvalidEnumKeyException $e) {
            return 'Unresolved bind type';
        }
    }

    public function setTechnologist(): JobTicket
    {
        $this->technologist = $this->jobs_prepress->flatMap(static function(JobPrepress $job) {
            return $job->executions;
        })->last();

        return $this;
    }

    public function setProof(): JobTicket
    {
        $this->proof = $this->jobs_prepress
            ->where('process_id', config('job-ticket.PROCESS.PROOF_ID'))
            ->values();

        return $this;
    }

    public function setTechnology(): JobTicket
    {
        $this->technology = $this->jobs_prepress->whereNotIn('process_id', [
            config('job-ticket.PROCESS.PROOF_ID'),
        ])->values();

        return $this;
    }

    public function addFakeJob(string $jobClass, array $properties): OptimaCollection
    {
        switch ($jobClass) {
            case JobPrepress::class:
                return $this->jobs_prepress->push(new JobPrepress(new FakeJob($properties)));
            case JobPrint::class:
                return $this->jobs_print->push(new JobPrint(new FakeJob($properties[0]), $properties[1], $properties[2]));
            case JobBindery::class:
                return $this->jobs_bindery->push(new JobBindery(new FakeJob($properties)));
            case JobsShipping::class:
                return $this->jobs_shipping->push(new JobsShipping(new FakeJob($properties)));
            default:
                throw new \InvalidArgumentException('Unrecognized job class.', 422);
        }
    }

    public function getSpineType(): int
    {
        $cover = $this->covers->filter(fn(CoverRequestParameters $cover) => $cover->spine !== null)->first();

        if ($cover) {
            return $cover->spine;
        }
        return -1;
    }

    private function extractSupernumeraryNote(string $notes_after): ?string
    {
        if (strpos($notes_after, config('job-ticket.SUPERNUMERARY_EDITION_KEYWORD')) !== false) {
            return trim(Str::after($notes_after, config('job-ticket.SUPERNUMERARY_EDITION_KEYWORD')));
        }
        return null;
    }

}
