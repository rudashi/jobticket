<?php

namespace Rudashi\JobTicket\Model;

use Illuminate\Support\Carbon;
use Rudashi\Profis\Classes\DataTransferObject;

class Order extends DataTransferObject
{
    public int $order_id;
    public string $order_number;
    public Carbon $order_deadline;
    public string $order_name;
    public ?string $isbn = null;
    public int $qty_total;
    public int $archived;
    public Carbon $order_date;
    public string $notes;
    public ?string $notes_internal = null;
    public ?string $notes_after = null;
    public int $customer_id;
    public ?string $binding_type = null;
    public ?string $binding_description = null;
    public int $selector_isbn_type;
    public int $selector_order_type;
    public int $selector_status_id;
    public float $mass_unit;
    public float $mass_total;
    public ?object $flaps = null;
    public string $customer_service_email;
    public string $customer_service_fullname;
    public string $customer_manager_email;
    public string $customer_manager_fullname;

    public function setOrderDeadlineProperty(string $property): Order
    {
        $this->order_deadline = new Carbon($property);

        return $this;
    }

    public function setOrderDateProperty(string $property): Order
    {
        $this->order_date = new Carbon($property);

        return $this;
    }

    public function setFlapsProperty(string $property = null): Order
    {
        if ($property && strpos($property, '-')) {
            $flaps = explode('-', $property);
            $this->flaps = (object) [
                'left'      => (int) $flaps[0],
                'right'     => (int) $flaps[1],
                'thickness' => (int) isset($flaps[2]) ? $flaps[2] : config('job-ticket.DEFAULT_FLAP_THICKNESS'),
            ];
        } else {
            $this->flaps = $property;
        }

        return $this;
    }

}
