<?php

namespace Rudashi\JobTicket\Model;

use ReflectionClass;
use ReflectionProperty;
use Rudashi\JobTicket\Model\Contracts\JobInterface;
use Rudashi\Optima\Classes\Collection;

class FakeJob implements JobInterface
{

    public function __construct(...$parameters)
    {
        if (is_array($parameters[0] ?? null)) {
            $parameters = $parameters[0];
        }
        $job = new Job($parameters);
        $reflect = new ReflectionClass($job);

        foreach ($reflect->getProperties(ReflectionProperty::IS_PUBLIC) as $reflectionProperty){
            tap($reflectionProperty, function(ReflectionProperty $property) use ($job) {
                $type = $property->getType();
                if (!$type) {
                    $this->{$property->getName()} = null;
                }
                if (!isset($job->{$property->getName()})) {
                    $this->{$property->getName()} = $type->allowsNull()
                        ? null
                        : $this->getDefaultValue($type->getName());
                } else {
                    $this->{$property->getName()} = $job->{$property->getName()};
                }
            });
        }
    }

    public function setExecutions(Collection $executions): FakeJob
    {
        return $this;
    }

    private function getDefaultValue(string $type)
    {
        switch ($type) {
            case 'int':
            case 'float':
                 return 0;
            case 'string':
                 return '';
            case 'array':
                 return [];
            case 'bool':
                 return false;
            case Collection::class:
                 return new $type();
            default:
                 return null;
        }
    }

}
