<?php

namespace Rudashi\JobTicket\Model;

use Rudashi\BookGenerator\App\Enums\SpineType;
use Rudashi\JobTicket\Model\Contracts\JobInterface;
use Rudashi\Optima\Classes\Collection as OptimaCollection;
use Rudashi\Profis\Classes\DataTransferObject;

class Job extends DataTransferObject implements JobInterface
{
    public const SQUARE_METERS = 11;

    public int $service_id;
    public int $task_id;
    public int $process_id;
    public int $task_type_id;
    public int $task_schema_id;

    public string $task_name;
    public ?string $machine_name = null;
    public string $service_name;
    public ?string $service_note = null;

    public ?string $quality = null;

    public ?int $sheet_verso = null;
    public ?int $sheet_recto = null;

    public int $machine_type_id;
    public ?int $order_quantity = null;
    public ?int $quantity_encore = null;
    public ?int $pieces = null;
    public int $pieces_width;
    public int $pieces_height;
    public ?int $sheets = null;

    public bool $printing_roller;
    public ?float $printing_width = null;
    public ?float $printing_height = null;

    public int $quantity;
    public int $quantity_proposal;
    public int $duplex;
    public ?int $quantity_done = null;

    public ?int $order_pages = null;
    public ?int $order_width = null;
    public ?int $order_height = null;

    public int $process_order;
    public int $process_asset_id;
    public int $batch;
    public ?int $spine = null;

    public ?string $execution_date = null;
    public string $service_status;

    /** @var OptimaCollection|\Rudashi\Profis\Classes\Models\Contracts\MaterialInterface[] */
    public OptimaCollection $materials;
    /** @var OptimaCollection|\Rudashi\Profis\Classes\Models\Contracts\ExecutionInterface[] */
    public OptimaCollection $executions;

    public int $article_id;
    public bool $sewing = false;
    public ?int $shipping_id = null;
    public ?int $step = null;

    public function setQualityProperty(int $value = null): Job
    {
        $this->quality = $value ? [442 => 'Standard', 443 => 'HD'][$value] : null;
        return $this;
    }

    public function setSheetRectoProperty(int $value = null): Job
    {
        $this->sheet_recto = $value ?: 0;
        return $this;
    }

    public function setPrintingRollerProperty(int $value = null): Job
    {
        $this->printing_roller = $value === self::SQUARE_METERS;
        return $this;
    }

    public function setDuplexProperty(?int $value = 1): Job
    {
        $this->duplex = $value === 2;
        return $this;
    }

    public function setSewingProperty(int $value): Job
    {
        $this->sewing = in_array($value, config('job-ticket.SELECTOR_SEWING'), true);
        return $this;
    }

    public function setSpineProperty(?int $value): Job
    {
        if ($value) {
            $this->spine = config('job-ticket.DICTIONARY.SPINE_FLAT_ID') === $value
                ? SpineType::FLAT
                : SpineType::ROUND;
        }
        return $this;
    }

    public function setExecutions(OptimaCollection $executions): Job
    {
        $this->executions = $executions;
        return $this;
    }

}
