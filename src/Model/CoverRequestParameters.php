<?php

namespace Rudashi\JobTicket\Model;

use Rudashi\BookGenerator\App\Enums\CoverType;
use Rudashi\BookGenerator\App\Enums\PrinterType;
use Rudashi\BookGenerator\App\Enums\SpiralPosition;
use Rudashi\Optima\Classes\Collection;
use Rudashi\Profis\Enums\BindType;

class CoverRequestParameters
{
    public string $name;
    public string $cover_type;
    public int $width;
    public int $height;
    public string $bind_type;

    public Collection $paper;

    public ?int $spine = null;
    public int $spine_encore = 0;
    public ?float $cardboard = null;

    public ?int $flaps = null;
    public ?int $flaps_left = null;
    public ?int $flaps_right = null;
    public ?int $flaps_thickness = null;

    public string $printer;
    public ?float $custom_spine = null;
    public ?array $spiral = null;

    public function __construct(Job $cover, Collection $papers)
    {
        $this->cover_type = $cover->task_type_id === config('profis.TASK.COVER') ? CoverType::COVER : CoverType::JACKET;
        $this->name = CoverType::getKey($this->cover_type);

        $this->paper = $papers->map(static function(Job $paper) {
            return (object) [
                'pages' => $paper->duplex ? $paper->order_pages : $paper->order_pages * 2,
                'type' => 1,
                'name' => $paper->materials->first()->name,
                'weight' => $paper->materials->first()->weight,
                'volume' => (float) number_format(($paper->materials->first()->thickness / $paper->materials->first()->weight), 2),
                'printer' => $paper->materials->first()->roll_width === null ? PrinterType::TONER : PrinterType::INK_JET,
            ];
        })->values();

        $this->spine = $cover->spine;
        $this->printer = $this->paper->where('pages', $this->paper->max('pages'))->first()->printer;
    }

    public function setBindingType(string $binding_type): self
    {
        $this->bind_type = BindType::getValue($binding_type);

        return $this;
    }

    public function setSpiral(JobBindery $spiral_qty = null, JobBindery $color = null, JobBindery $position = null): self
    {
        if (!$spiral_qty) {
            throw new \RuntimeException('Missing spiral quantity service.');
        }
        if (!$color) {
            throw new \RuntimeException('Missing spiral color service.');
        }
        if (!$position) {
            throw new \RuntimeException('Missing spiral position service.');
        }

        $this->spiral = [
            'color' => $color->name,
            'position' => $position->article_id === config('job-ticket.ARTICLE.SPIRAL_SHORT_ID')
                ? SpiralPosition::SHORT_SIDE
                : ($position->article_id === config('job-ticket.ARTICLE.SPIRAL_LONG_ID')
                    ? SpiralPosition::LONG_SIDE : null),
            'qty' => $spiral_qty->quantity,
        ];

        return $this;
    }

    public function setWidth(int $width): self
    {
        $this->width = $width;

        return $this;
    }

    public function setHeight(int $height): self
    {
        $this->height = $height;

        return $this;
    }

    public function setCardboard(?JobPrint $job): self
    {
        if ($job) {
            if (!$job->materials) {
                throw new \RuntimeException('No cardboard material in task: '.$job->task);
            }

            $this->cardboard =  $job->materials->thickness / 1000;
        }

        return $this;
    }

    public function setFlaps(Collection $covers, ?object $flaps): self
    {
        if ($flaps) {
            $this->flaps = 1;
            $this->flaps_left = $flaps->left;
            $this->flaps_right = $flaps->right;
            $this->flaps_thickness = $flaps->thickness;
        }
        if ($this->cover_type === CoverType::COVER && $covers->where('task_type_id', config('profis.TASK.JACKET'))->isNotEmpty()) {
            $this->flaps = null;
        }

        return $this;
    }

    public function setCustomSpine(float $custom_spine = null): self
    {
        if ($this->cover_type === CoverType::COVER && $custom_spine) {
            $this->custom_spine = $custom_spine;
        }

        return $this;
    }

}
