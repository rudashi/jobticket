<?php

namespace Rudashi\JobTicket\Model;

use Rudashi\Profis\Classes\Models\Execution;
use Rudashi\Profis\Traits\SetExecutionActions;

class JobExecution
{
    use SetExecutionActions;

    public int      $id;
    public ?string  $user_name;
    public ?string  $notes;
    public ?int     $quantity;
    public ?int     $quantity_todo;
    public ?string  $date_status;
    public ?string  $status;

    public function __construct(Execution $execution)
    {
        $this->id               = $execution->id;
        $this->user_name        = $execution->user_name;
        $this->notes            = $execution->notes;
        $this->quantity         = $execution->quantity;
        $this->quantity_todo    = $execution->quantity_todo;
        $this->date_status      = $execution->date_status;
        $this->status           = $execution->status;
        $this->actions          = $this->setActions($execution);
    }

}
