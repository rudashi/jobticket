<?php

namespace Rudashi\JobTicket\Model;

use Illuminate\Support\Carbon;
use Rudashi\Optima\Classes\Collection;
use Rudashi\Profis\Classes\Models\Shipping;
use Rudashi\Profis\Classes\Models\ShippingPackage;

class ShippingDelivery
{

    public int $shipping_id;
    public int $delivery_number;
    public string $delivery_method;
    public ?string $supplier;
    public string $quantity_setting;
    public ?string $quantity;
    public float $mass;
    public ?Carbon $shipping_date;
    public ?string $tracking_number;
    public string $name;
    public ?string $city;
    public ?string $postal_code;
    public ?string $street;
    public ?string $building_number;
    public ?string $suite_number;
    public ?string $country;
    public string $address;
    public ?string $phone_number;
    public ?string $description;
    public bool $main_surplus;
    public string $status;
    public ShippingPackage $package;
    public Collection $jobs;

    public function __construct(Shipping $shipping, Collection $jobs)
    {
        $this->shipping_id      = $shipping->shipping_id;
        $this->delivery_number  = $shipping->delivery_number;
        $this->delivery_method  = $shipping->delivery_method;
        $this->supplier         = $shipping->supplier;
        $this->quantity_setting = $shipping->quantity_setting;
        $this->quantity         = $shipping->quantity;
        $this->mass             = $shipping->mass;
        $this->shipping_date    = $shipping->shipping_date;
        $this->tracking_number  = $shipping->tracking_number;
        $this->name             = $shipping->name;
        $this->city             = $shipping->city;
        $this->postal_code      = $shipping->postal_code;
        $this->street           = $shipping->street;
        $this->building_number  = $shipping->building_number;
        $this->suite_number     = $shipping->suite_number;
        $this->country          = $shipping->country;
        $this->address          = $this->setAddress($shipping);
        $this->phone_number     = $shipping->phone_number;
        $this->description      = $shipping->description;
        $this->main_surplus     = $shipping->main_surplus;
        $this->package          = $shipping->package->setServices($jobs->mapInto(JobsShippingDelivery::class)->all());
        $this->status           = $shipping->status;
    }

    private function setAddress(Shipping $shipping): string
    {
        return trim(implode('', [
            $shipping->street,
            ' ',
            $shipping->building_number,
            $shipping->suite_number ? '/' : null,
            $shipping->suite_number,
            ', ',
            $shipping->postal_code,
            ' ',
            $shipping->city
        ]));
    }

}
