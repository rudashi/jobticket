<?php

namespace Rudashi\JobTicket\Model\Contracts;

use Rudashi\Optima\Classes\Collection as OptimaCollection;

/**
 * @property int $service_id
 * @property int $task_id
 * @property int $process_id
 * @property int $task_type_id
 * @property int $task_schema_id

 * @property string $task_name
 * @property string|null $machine_name
 * @property string $service_name
 * @property string|null $service_note

 * @property string|null $quality

 * @property int|null $sheet_verso
 * @property int|null $sheet_recto

 * @property int $machine_type_id
 * @property int|null $order_quantity
 * @property int|null $quantity_encore
 * @property int|null $pieces
 * @property int $pieces_width
 * @property int $pieces_height
 * @property int|null $sheets

 * @property bool $printing_roller
 * @property ?float $printing_width
 * @property ?float $printing_height

 * @property int $quantity
 * @property int $quantity_proposal
 * @property int $duplex
 * @property int|null $quantity_done

 * @property int|null $order_pages
 * @property int|null $order_width
 * @property int|null $order_height

 * @property int $process_order
 * @property int $process_asset_id
 * @property int $batch

 * @property string|null $execution_date
 * @property string $service_status

 * @property OptimaCollection $materials
 * @property OptimaCollection $executions

 * @property int $article_id
 * @property bool $sewing
 * @property int|null shipping_id
 * @property int|null $step
 */
interface JobInterface
{

    public function setExecutions(OptimaCollection $executions);

}
