<?php

namespace Rudashi\JobTicket\Model;

use Illuminate\Support\Collection;
use Illuminate\Support\MessageBag;
use Rudashi\JobTicket\Model\Contracts\JobInterface;
use Rudashi\Optima\Classes\Collection as OptimaCollection;
use Rudashi\Profis\Classes\Models\Contracts\ColorInterface;
use Rudashi\Profis\Classes\Models\Contracts\MaterialInterface;
use Rudashi\Profis\Enums\BindType;

class JobPrint
{

    public int $id;
    public int $task_id;
    public int $process_id;
    public int $order;
    public string $name;
    public string $task;
    public ?string $quality = null;
    public ?string $machine = null;
    public ?int $step = null;
    public int $quantity;
    public string $colors;
    public array $colorList;
    public ?int $sets = null;
    public string $format;
    public object $net;
    public ?object $gross = null;
    public object $pieces;
    public ?int $pages = null;
    public ?string $notes = null;
    public ?int $quantity_done = null;
    public ?string $execution_date = null;
    public string $status;
    public OptimaCollection $executions;
    public ?MaterialInterface $materials = null;
    public MessageBag $alerts;

    public function __construct(JobInterface $job, Collection $colors, array $options)
    {
        $material = $job->materials->first();

        $this->id               = $job->service_id;
        $this->task_id          = $job->task_id;
        $this->process_id       = $job->process_id;
        $this->order            = $job->process_order;
        $this->name             = $job->service_name;
        $this->task             = $job->task_name;
        $this->quality          = $job->quality;
        $this->machine          = $job->machine_name;
        $this->step             = $job->step;
        $this->colors           = $this->setColors($job, $options);
        $this->colorList        = $this->attachColorList($job, $colors);
        $this->quantity         = $this->getQuantityForPrint($job);
        $this->sets             = $this->getSets($job, $options);
        $this->format           = $this->getPrintFormat($job);
        $this->pages            = $job->order_pages;
        $this->pieces           = $this->getPieces($job);
        $this->net              = $this->getNet($job);
        $this->gross            = $this->getGross($job, $options['jobs_main'], $options);
        $this->notes            = $job->service_note;
        $this->quantity_done    = $job->quantity_done;
        $this->execution_date   = $job->execution_date;
        $this->status           = $job->service_status;
        $this->executions       = $job->executions;
        $this->materials        = $material;
        $this->alerts           = new MessageBag($this->setAlerts($job, $options));
    }

    private function attachColorList(JobInterface $job, Collection $colors): array
    {
        if ($job->process_id === config('job-ticket.PROCESS.OFFSET_ID')) {
            return $colors->filter(static function(ColorInterface $color) use ($job) {
                return $color->task_id === $job->task_id;
            })->groupBy('side')->toArray();
        }
        return [];
    }

    private function getQuantityForPrint(JobInterface $job): int
    {
        return $job->duplex ? ceil($job->sheets / 2) : $job->sheets;
    }

    private function getPrintFormat(JobInterface $job): string
    {
        if ($job->printing_roller) {
            return $job->printing_width;
        }
        return $job->printing_width.'x'.$job->printing_height;
    }

    private function getSets(JobInterface $job, array $options): ?int
    {
        if ($job->machine_type_id === 0) {
            return null;
        }

        $sets = $this->setSets($job, $options);

        if ($job->pieces % 2 === 0 && (in_array($job->task_schema_id, config('job-ticket.SCHEMA.ENDPAPER'), true) === true)) {
            $sets *= 2;
        }

        return $sets;
    }

    private function getNet(JobInterface $job): object
    {
        return (object) [
            'width' => $job->order_width,
            'height' => $job->order_height,
        ];
    }

    private function getGross(JobInterface $job, JobInterface $main, array $options): ?object
    {
        if (in_array($job->task_schema_id, config('job-ticket.SCHEMA.ENDPAPER'), true) === false) {
            return null;
        }

        if ($options['binding_type'] === BindType::getKey(BindType::HARDCOVER_WIRE_O)) {
            return (object) [
                'width' => $main->order_width,
                'height' => $main->order_height,
            ];
        }

        $width = ($main->order_width * 2) + 10;
        $height = $main->order_height + 10;

        if ($main->printing_roller) {
            $fiber_correct = (int) $main->printing_height === (int) $main->order_height + 6;
            $height += $fiber_correct ? 5 : 0;

            if ($options['binding_type'] === BindType::getKey(BindType::HARDCOVER_SEWN)) {
                $width = $width > $main->printing_width ? $main->printing_width : $width;
                $height = $fiber_correct ? $height : floor($main->printing_width / 2);

                if ($options['universe_web']->isNotEmpty()) {
                    $height += 7;
                }
            }

            if ($options['binding_type'] === BindType::getKey(BindType::HARDCOVER_GLUED)) {
                if ($options['book_line']->contains($job->task_id)) {
                    $height = max($height, 190);
                }
                if ($options['cutter']->contains($job->task_id)) {
                    $height = max($height, 135);
                }

            }

            return (object) [
                'width' => (int) $width,
                'height' => (int) $height,
            ];
        }

        if ($main->printing_width > 350 && $main->printing_height > 350) {
            return (object) [
                'width' => (int) $width,
                'height' => (int) $height,
            ];
        }

        $width = $width > $main->printing_width ? $main->printing_width : $width;

        if ($main->pieces_width > $main->pieces_height) {
            return (object) [
                'width' => (int) $main->printing_width,
                'height' => (int) $main->printing_height,
            ];
        }
        if ($main->pieces_width === $main->pieces_height) {
            $height = $height > ($main->printing_height / 2) ? floor($main->printing_height / 2) : $height;
        }

        return (object) [
            'width' => (int) $width,
            'height' => (int) $height,
        ];
    }

    private function getPieces(JobInterface $job): object
    {
        return (object) [
            'quantity' => $job->pieces,
            'width' => $job->pieces_width,
            'height' => $job->pieces_height,
        ];
    }

    private function setColors(JobInterface $job, array $options): string
    {
        if ($options['refresh_line']->contains($job->task_id)) {
            return '4+4';
        }
        return $job->sheet_verso.'+'.$job->sheet_recto;
    }

    private function setAlerts(JobInterface $job, array $options): array
    {
        $messages = [
            'info' => [],
            'warning' => [],
            'error' => [],
        ];
        $material = $job->materials->first();

        if ($options['personalization']->contains($job->task_id)) {
            $messages['info']['personalization'] = 'personalization';
        }
        if ($material !== null && $material->mass === 0) {
            $messages['error']['material_mass'] = 'No material weight';
        }
        if ($job->sewing && $job->pieces === 1) {
            $messages['error']['sewing_piece'] = 'Sewing cannot be done in one piece';
        }

        return $messages;
    }

    private function setSets(JobInterface $job, array $options): int
    {
        if ($options['personalization']->contains($job->task_id)) {
            return 1;
        }

        if ($options['book_line']->contains($job->task_id)) {
            return $job->order_quantity + $job->quantity_encore;
        }

        if ($job->pieces) {
            if ($options['binding_type'] !== BindType::getKey(BindType::HARDCOVER_WIRE_O) && $job->task_schema_id === config('job-ticket.SCHEMA.HARDCOVER_WIRE_O_COVER_PRINT')) {
                return ceil(($job->order_quantity / $job->pieces * 2) + $job->quantity_encore);
            }
            if ($job->sewing) {
                return ceil(($job->order_quantity / $job->pieces * 2) + $job->quantity_encore);
            }
            return ceil(($job->order_quantity / $job->pieces) + $job->quantity_encore);
        }
        return $job->sheets;
    }

}
