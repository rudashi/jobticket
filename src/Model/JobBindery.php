<?php

namespace Rudashi\JobTicket\Model;

use Rudashi\JobTicket\Model\Contracts\JobInterface;
use Rudashi\Optima\Classes\Collection as OptimaCollection;

class JobBindery
{

    public int $id;
    public int $task_id;
    public int $process_id;
    public int $article_id;
    public int $order;
    public string $name;
    public string $task;
    public int $quantity;
    public ?string $notes = null;
    public ?int $quantity_done = null;
    public ?string $execution_date = null;
    public string $status;
    public OptimaCollection $executions;

    public function __construct(JobInterface $job, array $options = [])
    {
        $this->id               = $job->service_id;
        $this->task_id          = $job->task_id;
        $this->process_id       = $job->process_id;
        $this->order            = $job->process_order;
        $this->name             = $job->service_name;
        $this->task             = $job->task_name;
        $this->article_id       = $job->article_id;
        $this->quantity         = $this->getQuantity($job);
        $this->notes            = $job->service_note;
        $this->quantity_done    = $job->quantity_done;
        $this->execution_date   = $job->execution_date;
        $this->status           = $job->service_status;
        $this->executions       = $job->executions;
    }

    private function getQuantity(JobInterface $job): int
    {
        if ($job->printing_roller) {
            return $job->order_quantity + $job->quantity_encore;
        }
        return $job->quantity_proposal;
    }

}
