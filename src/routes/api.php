<?php

use Illuminate\Support\Facades\Route;
use Rudashi\JobTicket\Controllers\FileController;
use Rudashi\JobTicket\Controllers\JobTicketController;
use Rudashi\JobTicket\Controllers\JobTicketPdfController;
use Rudashi\Profis\Controllers\ActionController;

Route::group(['prefix' => 'api'], static function () {

    Route::middleware(config('sam-admin.guard-api'))->group( static function() {

        Route::group(['prefix' => 'profis'], static function () {

            Route::group(['prefix' => 'job-ticket'], static function () {
                Route::post('/', [JobTicketController::class, 'search'])->name('api.profis.job-ticket.search');
                Route::get('{id}', [JobTicketController::class, 'show'])->name('api.profis.job-ticket.show');
                Route::post('{id}', ActionController::class)->middleware('permission:job-ticket.action')->name('api.profis.job-ticket.action');
                Route::get('{id}/messages', [JobTicketController::class, 'messages'])->name('api.profis.job-ticket.messages');
                Route::get('{id}/pdf', [JobTicketPdfController::class, 'streamLabel'])->name('api.profis.job-ticket.label');
                Route::get('{id}/pdf/shipping', [JobTicketPdfController::class, 'streamShipping'])->name('api.profis.job-ticket.shipping');

                Route::get('{id}/file', [FileController::class, 'show']);
                Route::post('{id}/file', [FileController::class, 'upload']);
                Route::delete('{id}/file/{fileId}', [FileController::class, 'remove'])->where(['fileId' => '[0-9]+']);
            });

        });
    });

});
