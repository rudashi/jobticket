<?php

namespace Rudashi\JobTicket\Controllers;

use App\Http\Controllers\Controller;
use Rudashi\JobTicket\Enums\OrderPhraseType;
use Rudashi\JobTicket\Repositories\Contracts\JobTicketRepositoryInterface;
use Rudashi\JobTicket\Resources\JobTicketResource;
use Rudashi\Profis\Classes\Filters;
use Rudashi\Profis\Enums\Status;
use Rudashi\Profis\Requests\SearchRequest;
use Rudashi\Profis\Resources\MessageCollection;
use Rudashi\Profis\Resources\OrderCollection;
use Totem\SamCore\App\Traits\CanRequestQuery;

class JobTicketController extends Controller
{
    use CanRequestQuery;

    private JobTicketRepositoryInterface $repository;

    public function __construct(JobTicketRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function search(SearchRequest $request): OrderCollection
    {
        return new OrderCollection($this->getFromRequestQuery(
            $this->repository->findOrdersByPhrase(
                $request->post('type', OrderPhraseType::NUMBER),
                $request->input('phrase'),
                new Filters([
                    'archived' => $request->input('archived'),
                    'not_status' => Status::Canceled
                ])
            ),
            ['sort' => ['order_id' => 'desc']]
        ));
    }

    public function show(int $order_id): JobTicketResource
    {
        return new JobTicketResource($this->repository->create($order_id));
    }

    public function messages(int $order_id): MessageCollection
    {
        return new MessageCollection(
            $this->repository->fetchMessages($order_id)
        );
    }

}
