<?php

namespace Rudashi\JobTicket\Controllers;

use App\Http\Controllers\Controller;
use Barryvdh\Snappy\PdfWrapper;
use Illuminate\Foundation\Application;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use Rudashi\JobTicket\Repositories\Contracts\JobTicketRepositoryInterface;

class JobTicketPdfController extends Controller
{

    private JobTicketRepositoryInterface $repository;
    private Application $app;

    public function __construct(JobTicketRepositoryInterface $repository, Application $application)
    {
        $this->repository = $repository;
        $this->app = $application;
    }

    public function streamLabel(int $order_id): Response
    {
        return $this->newPDF()->loadView('job-ticket::pdf-label', [
            'order' => $this->repository->getOrder($order_id),
            'url' => config('app.url').'/t/job-ticket/'.$order_id,
        ])
            ->setOptions([
                'dpi' => 150,
                'margin-bottom' => 0,
                'margin-top' => 0,
                'margin-left' => 0,
                'margin-right' => 0,
                'page-height' => 100,
                'page-width' => 100,
                'enable-smart-shrinking' => false,
            ])
            ->inline($order_id . '.pdf');
    }

    public function streamShipping(int $order_id): Response
    {
        return $this->newPDF()->loadView('job-ticket::pdf-shipping', [
            'order' => $this->repository->create($order_id),
            'date' => Carbon::now(),
        ])
            ->setPaper('A4')
            ->inline(Str::kebab('job-ticket-shipping-' . Carbon::now()) . '.pdf');
    }

    private function newPDF(): PdfWrapper
    {
        return $this->app->make(PdfWrapper::class);
    }

}
