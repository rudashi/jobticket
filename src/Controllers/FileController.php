<?php

namespace Rudashi\JobTicket\Controllers;

use Rudashi\JobTicket\Model\JobTicket;
use Totem\SamCore\App\Requests\FileUploadRequest;
use Totem\SamCore\App\Resources\ApiResource;
use Totem\SamCore\App\Resources\FileResource;
use Totem\SamCore\App\Resources\FileCollection;
use Totem\SamCore\App\Repositories\Contracts\FileInterfaceRepository;
use Totem\SamCore\App\Controllers\ApiController;

class FileController extends ApiController
{

    public function __construct(FileInterfaceRepository $repository)
    {
        $this->repository = $repository;
    }

    public function show(int $id): FileCollection
    {
        return new FileCollection($this->repository->findByModelUuid($id, JobTicket::class));
    }

    public function upload(int $id, FileUploadRequest $request): ApiResource
    {
        try {
            return new FileResource($this->repository->save(
                $request,
                'job_ticket',
                JobTicket::class,
                $id
            ));
        } catch (\Exception $exception) {
            return $this->response($this->error(422, $exception->getMessage()));
        }
    }

    public function remove(int $id, int $file_id): FileResource
    {
        return new FileResource(
            $this->repository->fileRemove($file_id)
        );
    }

}
