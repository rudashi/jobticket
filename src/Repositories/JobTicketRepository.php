<?php

namespace Rudashi\JobTicket\Repositories;

use InvalidArgumentException;
use Rudashi\BookGenerator\App\Enums\SpineType;
use Rudashi\JobTicket\Enums\OrderPhraseType;
use Rudashi\JobTicket\Model\Contracts\JobInterface;
use Rudashi\JobTicket\Model\JobBindery;
use Rudashi\JobTicket\Model\JobsShipping;
use Rudashi\JobTicket\Model\JobTicket;
use Rudashi\JobTicket\Model\ShippingDelivery;
use Rudashi\JobTicket\Traits\ProfisQueries;
use Rudashi\Profis\Classes\Models\Contracts\ShippingInterface;
use Rudashi\Optima\Classes\Collection;
use Rudashi\Profis\Classes\Columns;
use Rudashi\Profis\Classes\Filters;
use Rudashi\Profis\Classes\Models\Contracts\OrderInterface;
use Rudashi\Profis\Classes\Models\Contracts\MaterialInterface;
use Rudashi\Profis\Repositories\Contracts\OrderRepositoryInterface;
use Rudashi\JobTicket\Repositories\Contracts\JobTicketRepositoryInterface;

class JobTicketRepository implements JobTicketRepositoryInterface
{

    use ProfisQueries;

    private OrderRepositoryInterface $profis;
    private JobTicket $model;

    public function __construct(OrderRepositoryInterface $profis, JobTicket $model)
    {
        $this->profis = $profis;
        $this->model = $model;
    }

    public function findOrdersByPhrase(string $type, string $phrase, Filters $filters): Collection
    {
        switch ($type) {
            case OrderPhraseType::NUMBER:
                return $this->profis->findOrdersByNumberPhrase($phrase, $filters);
            case OrderPhraseType::CUSTOMER:
                return $this->profis->findOrdersByCustomerPhrase($phrase, $filters);
            case OrderPhraseType::TITLE:
                return $this->profis->findOrdersByTitlePhrase($phrase, $filters);
            case OrderPhraseType::ALL:
                return $this->profis->findOrdersByPhrase($phrase, $filters);
            default:
                throw new InvalidArgumentException('Unrecognized search type parameter.', 422);
        }
    }

    public function fetchMessages(int $order_id): Collection
    {
        return $this->profis->fetchMessages($order_id);
    }

    public function getOrder(int $order_id): OrderInterface
    {
        return $this->profis->findOrder(
            new Filters([
                'ids' => [$order_id],
            ]),
            new Columns([
                'customer',
                'customer_service'
            ])
        );
    }

    public function create(int $order_id): JobTicket
    {
        $order              = $this->findOrder($order_id);
        $jobs               = $this->fetchJobs($order_id);
        $materials          = $this->profis->fetchMaterials($jobs->pluck('task_id'));
        $shipping_delivery  = $this->profis->getShippingAddresses($order_id);
        $customers          = $this->profis->fetchCustomers([
            $order->customer_id,
            $shipping_delivery->getColumnsFromCollection(['supplier_id']),
        ]);
        $selectors = $this->profis->getSelectors([
            $order->selector_isbn_type,
            $order->selector_order_type,
            $order->selector_status_id,
            $shipping_delivery->getColumnsFromCollection(['status_id', 'delivery_method_id']),
        ]);

        $jobs_print     = $this->attachMaterialsToTask(
            $jobs->where('process_asset_id', $this->profis::getPrintingId()),
            $materials
        );
        $jobs_bindery   = $jobs->where('process_asset_id', $this->profis::getBinderyId())->where('process_order', '<', config('job-ticket.PROCESS.ORDER_SHIPPING_LIMIT')[0]);
        $jobs_for_inner = $jobs_print->where('task_type_id', config('profis.TASK.INNER'));
        $job_main = $this->setMainJobs($jobs_print, $jobs_for_inner);

        $this->prepareShippingDelivery(
            $shipping_delivery,
            $selectors,
            $customers,
            $jobs->where('shipping_id')->whereBetween('process_order', config('job-ticket.PROCESS.ORDER_SHIPPING_LIMIT'))
        );

        $this->model
            ->setOrder($order)
            ->setCustomer($customers->get($order->customer_id))
            ->setPublication($selectors->get($order->selector_isbn_type), $order->isbn)
            ->setOrderStatus($selectors->get($order->selector_status_id))
            ->setOrderType($selectors->get($order->selector_order_type))
            ->setJobMain($job_main)
            ->setFormat($job_main->where('order_pages', $job_main->max('order_pages'))->first())
            ->setPages($jobs_print, $jobs_for_inner)
            ->setPersonalization($jobs)
            ->setJobsBindery($jobs_bindery)
            ->setJobsPrint(
                $jobs_print,
                $this->profis->fetchColors($jobs_print->pluck('task_id'))
            )
            ->setCoversCalculation(
                $jobs_print->whereIn('task_type_id', [config('profis.TASK.COVER'), config('profis.TASK.JACKET')]),
                $jobs_for_inner
            )
        ;

        $this->addPrepress($jobs);
        $this->addShipping($jobs, $shipping_delivery);

        $this->registerFakeJobs();

        $this->model->validateJobTicket();

        return $this->model;
    }

    private function attachMaterialsToTask(Collection $tasks, \Illuminate\Support\Collection $materials): Collection
    {
        return $tasks->map(static function(JobInterface $job) use ($materials) {
            $job->materials = $materials->filter(static function(MaterialInterface $material) use ($job) {
                return $job->task_id === $material->task_id;
            });
            return $job;
        });
    }

    private function prepareShippingDelivery(Collection $shipping, Collection $selectors, Collection $customers, Collection $jobs): void
    {
        $dictionaries = $this->profis->getDictionaries(
            $shipping->getColumnsFromCollection(['package_method_id', 'package_2_method_id', 'package_3_method_id', 'package_4_method_id', 'package_id'])
        );

        $shipping->transform(static function (ShippingInterface $item) use ($dictionaries, $selectors, $customers, $jobs) {
            $item->setStatus($selectors->get($item->status_id));
            $item->setDeliveryMethod($selectors->get($item->delivery_method_id));
            $item->setPackage(
                $dictionaries->get($item->package_id),
                $dictionaries->get($item->package_method_id),
                $dictionaries->get($item->package_2_method_id),
                $dictionaries->get($item->package_3_method_id),
                $dictionaries->get($item->package_4_method_id),
            );
            if ($item->supplier_id) {
                $item->setSupplier($customers->get($item->supplier_id)->name);
            }

            return new ShippingDelivery($item, $jobs->where('shipping_id', $item->shipping_id)->sortBy('process_order'));
        });
    }

    private function setMainJobs(Collection $jobs_print, Collection $jobs_for_inner): Collection
    {
        return $jobs_for_inner->isEmpty() ? $jobs_print : $jobs_for_inner;
    }

    private function addPrepress(Collection $jobs): void
    {
        $this->model
            ->setJobPrepress($jobs->where('process_asset_id', $this->profis::getPrepressId()))
            ->setTechnologist()
            ->setProof()
            ->setTechnology();
    }

    private function addShipping(Collection $jobs, Collection $shipping_delivery): void
    {
        $this->model->setJobsShipping(
            $jobs->where('process_asset_id', $this->profis::getBinderyId())
                ->whereBetween('process_order', config('job-ticket.PROCESS.ORDER_SHIPPING_LIMIT'))
        )->setShippingDelivery($shipping_delivery);
    }

    private function registerFakeJobs(): void
    {
        if ($this->model->customer->email_warehouse) {
            $this->model->addFakeJob(JobsShipping::class, [
                'service_name' => __('Delivery notification'),
                'service_note' => $this->model->customer->email_warehouse,
                'sheets' => 0
            ]);
        }
        if ($this->model->flaps && (int) $this->model->flaps->thickness !== 1) {
            $this->model->addFakeJob(JobBindery::class, [
                'service_name' => __('Custom flaps'),
                'service_note' => $this->model->flaps->thickness.' mm',
            ]);
        }
        if (($spine = $this->model->getSpineType()) >= 0) {
            $this->model->addFakeJob(JobBindery::class, [
                'service_name' => __('Spine type'),
                'service_note' => SpineType::fromValue($spine)->description,
            ]);
        }
    }

}
