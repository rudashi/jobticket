<?php

namespace Rudashi\JobTicket\Repositories\Contracts;

use Rudashi\JobTicket\Model\JobTicket;
use Rudashi\Optima\Classes\Collection;
use Rudashi\Profis\Classes\Filters;
use Rudashi\Profis\Classes\Models\Contracts\OrderInterface;

interface JobTicketRepositoryInterface
{

    public function create(int $order_id): JobTicket;

    public function fetchMessages(int $order_id): Collection;

    public function findOrdersByPhrase(string $type, string $phrase, Filters $filters): Collection;

    public function getOrder(int $order_id): OrderInterface;

}
