<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Database\Migrations\Migration;

class JobTicket extends Migration
{

    /**
     * @return void
     * @throws Exception
     */
    public function up() : void
    {
        try {
            Artisan::call('db:seed', [
                '--class' => \Rudashi\JobTicket\Database\Seeds\PermissionSeeder::class,
            ]);
        } catch (PDOException $ex) {
            $this->down();
            throw $ex;
        }
    }

    /**
     * @return void
     * @throws Exception
     */
    public function down() : void
    {
        (new Rudashi\JobTicket\Database\Seeds\PermissionSeeder)->down();
    }
}
