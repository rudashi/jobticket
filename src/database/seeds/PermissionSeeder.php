<?php

namespace Rudashi\JobTicket\Database\Seeds;

use Totem\SamAcl\Database\PermissionTraitSeeder;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    use PermissionTraitSeeder;

    /**
     * Array of permissions.
     * %action  : view|create|edit|show|delete|modify
     * %name    : translatable from JSON
     *
     * @return array
     *  [
     *      'slug' => 'roles.modify',
     *      'name' => 'Can Modify Roles',
     *      'description' => 'Can modify roles',
     *  ]
     */
    public function permissions(): array
    {
        return [
            [
                'slug' => 'job-ticket',
                'name' => 'Display Job ticket',
                'description' => 'Display job ticket',
            ],
            [
                'slug' => 'job-ticket.pdf',
                'name' => 'Can create job ticket PDF',
                'description' => 'Can create label with job ticket',
            ],
            [
                'slug' => 'job-ticket.action',
                'name' => 'Can perform execution',
                'description' => 'Can perform execution action from job ticket',
            ],
            [
                'slug' => 'job-ticket.schedule',
                'name' => 'Can View Production Schedule',
                'description' => 'Can view the production schedule',
            ],
        ];
    }

}
