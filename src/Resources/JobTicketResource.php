<?php

namespace Rudashi\JobTicket\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Rudashi\JobTicket\Model\JobPrint;
use Rudashi\JobTicket\Model\JobsShippingDelivery;
use Rudashi\JobTicket\Model\ShippingDelivery;
use Rudashi\Profis\Classes\Models\Contracts\MaterialInterface;

/** @property \Rudashi\JobTicket\Model\JobTicket $resource */
class JobTicketResource extends JsonResource
{

    public function toArray($request) : array
    {
        return [
            'order_id'              => $this->resource->order_id,
            'order_number'          => $this->resource->order_number,
            'status'                => $this->resource->status,
            'order_type'            => $this->resource->order_type,
            'binding_code'          => $this->resource->getBinding('value'),
            'binding_type'          => $this->resource->getBinding('description'),
            'binding_description'   => $this->resource->binding_description,
            'order_date'            => $this->resource->order_date->format('Y-m-d H:i:s'),
            'order_deadline'        => $this->resource->order_deadline->format('Y-m-d'),

            'customer'              => $this->resource->customer->name,
            'customer_manager'      => $this->resource->customer_manager,
            'order_name'            => $this->resource->order_name,
            'customer_service'      => $this->resource->customer_service,
            'publication_number'    => $this->resource->publication_number,
            'technologist'          => ExecutionResource::make($this->resource->technologist),

            'format'                => $this->resource->format,
            'pages'                 => $this->resource->pages,
            'qty_total'             => $this->resource->qty_total,

            'technology'            => $this->resource->technology->map(fn($job) => $this->jobSimpleResource($job)),
            'proof'                 => $this->resource->proof->map(fn($job) => $this->jobSimpleResource($job)),
            'print'                 => $this->resource->jobs_print->map(fn($job) => $this->printResource($job)),
            'bindery'               => $this->resource->jobs_bindery->map(fn($job) => $this->jobResource($job)),
            'shipping'              => $this->resource->jobs_shipping->map(fn($job) => $this->jobResource($job)),
            'delivery'              => $this->resource->shipping_delivery->map(fn($shipping) => $this->delivery($shipping)),

            'mass_unit'             => $this->resource->mass_unit,
            'mass_total'            => $this->resource->mass_total,
            'notes'                 => $this->resource->notes,
            'notes_supernumerary'   => $this->resource->notes_supernumerary,

            'alerts'                => $this->resource->alerts,

            'covers'                => CoverResource::collection($this->resource->covers),
        ];
    }

    private function jobSimpleResource(object $job): array
    {
        return [
            'process_id'    => $job->process_id,
            'task'          => $job->task,
            'name'          => $job->name,
            'notes'         => $job->notes,
            'quantity'      => $job->quantity,
            'executions'    => ExecutionResource::collection($job->executions),
            'status'        => $job->status,
        ];
    }

    private function printResource(JobPrint $job): array
    {
        return [
            'process_id'    => $job->process_id,
            'task'          => $job->task,
            'quality'       => $job->quality,
            'machine'       => $job->machine,
            'step'          => $job->step,
            'material'      => $this->material($job->materials),
            'quantity'      => $job->quantity,
            'sets'          => $job->sets,
            'format'        => $job->format,
            'net'           => $job->net,
            'gross'         => $job->gross,
            'pieces'        => $job->pieces,
            'pages'         => $job->pages,
            'colors'        => $job->colors,
            'colorList'     => $job->colorList,
            'notes'         => $job->notes,
            'quantity_done' => $job->quantity_done,
            'alerts'        => $job->alerts->getMessages(),
            'executions'    => ExecutionResource::collection($job->executions),
            'status'        => $job->status,
        ];
    }

    private function jobResource(object $job): array
    {
        return [
            'id'            => $job->id,
            'process_id'    => $job->process_id,
            'task'          => $job->task,
            'name'          => $job->name,
            'notes'         => $job->notes,
            'quantity'      => $job->quantity,
            'quantity_done' => $job->quantity_done,
            'executions'    => ExecutionResource::collection($job->executions),
            'status'        => $job->status,
        ];
    }

    private function delivery(ShippingDelivery $job): array
    {
        return [
            'delivery_number'   => $job->delivery_number,
            'delivery_method'   => $job->delivery_method,
            'supplier'          => $job->supplier,
            'quantity_setting'  => $job->quantity_setting,
            'quantity'          => $job->quantity,
            'mass'              => $job->mass,
            'shipping_date'     => $job->shipping_date ? $job->shipping_date->format('Y-m-d') : null,
            'tracking_number'   => $job->tracking_number,
            'recipient'         => $job->name,
            'address'           => $job->address,
            'phone'             => $job->phone_number,
            'notes'             => $job->description,
            'main_surplus'      => $job->main_surplus,
            'package'           => [
                'main' => $job->package->main,
                'methods' => $job->package->methods,
                'services' => array_values(array_map(function(JobsShippingDelivery $job) {
                    return $this->jobSimpleResource($job);
                }, $job->package->services))
            ],
            'status'            => $job->status,
        ];
    }

    private function material(MaterialInterface $material = null): ?array
    {
        if ($material === null) {
            return null;
        }
        return [
            'name'          => $material->name,
            'weight'        => $material->weight,
            'thickness'     => $material->thickness,
            'volume'        => (float) number_format(($material->thickness / $material->weight), 2),
            'roll'          => $material->roll_width,
            'width'         => $material->sheet_width,
            'height'        => $material->sheet_height,
            'mass'          => $material->resource_id === config('profis.MATERIAL.CANVAS_ROLL') ? $material->quantity : $material->mass,
            'mass_unit'     => $material->resource_id === config('profis.MATERIAL.CANVAS_ROLL') ? 'MB' : 'kg',
            'entrusted'     => $material->entrusted,
        ];
    }

}
