<?php

namespace Rudashi\JobTicket\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property \Rudashi\JobTicket\Model\JobExecution $resource
 */
class ExecutionResource extends JsonResource
{

    public function toArray($request): array
    {
        return [
            'id'            => $this->resource->id,
            'user_name'     => $this->resource->user_name,
            'notes'         => $this->resource->notes,
            'quantity'      => $this->resource->quantity,
            'quantity_todo' => $this->resource->quantity_todo,
            'date_status'   => $this->resource->date_status,
            'status'        => $this->resource->status,
            'actions'       => $this->resource->actions,
        ];
    }

}
