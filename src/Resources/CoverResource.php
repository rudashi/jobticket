<?php

namespace Rudashi\JobTicket\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property \Rudashi\JobTicket\Model\CoverRequestParameters $resource
 */
class CoverResource extends JsonResource
{

    public function toArray($request): array
    {
        return [
            'name'              => $this->resource->name,
            'cover_type'        => $this->resource->cover_type,
            'width'             => $this->resource->width,
            'height'            => $this->resource->height,
            'bind_type'         => $this->resource->bind_type,
            'paper'             => $this->resource->paper,
            'spine_encore'      => $this->resource->spine_encore,
            'spine'             => $this->when($this->resource->spine !== null, $this->resource->spine),
            'spine_custom'      => $this->when($this->resource->custom_spine, $this->resource->custom_spine),
            'cardboard'         => $this->when($this->resource->cardboard, $this->resource->cardboard),
            'flaps'             => $this->when($this->resource->flaps, $this->resource->flaps),
            'flaps_left'        => $this->when($this->resource->flaps, $this->resource->flaps_left),
            'flaps_right'       => $this->when($this->resource->flaps, $this->resource->flaps_right),
            'flaps_thickness'   => $this->when($this->resource->flaps, $this->resource->flaps_thickness),
            'printer'           => $this->resource->printer,
            'spiral'            => $this->when($this->resource->spiral !== null, $this->resource->spiral),
        ];
    }

}
