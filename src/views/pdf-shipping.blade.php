<!DOCTYPE html>
<html lang="pl">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Job Ticket - Shipping</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&subset=latin-ext" rel="stylesheet">
    <style>
        * {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        html, body {
            height: 100%;
            margin: 0;
            font-size: 1em;
            font-family: 'Roboto', sans-serif;
        }

        .page--break {
            page-break-after: always;
        }

        .table {
            width: 100%;
            display: flex;
            display: -webkit-box;
            flex-direction: column;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            font-size: 2em;
        }

        .table .tbody {
            display: flex;
            display: -webkit-box;
            flex-direction: column;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
        }

        .table .tbody .tr {
            width: 100%;
            display: -webkit-box;
            display: flex;
        }

        .table .tbody .td {
            padding: 2mm 0 2mm 0;
        }

        .table .tbody .td.left {
            width: 30%;
            font-weight: 700;
            text-align: right;
            padding: 2mm 5mm 2mm 0;
        }

        .table .tbody .td.right {
            width: 70%;
        }

        .table--box {
            padding-top: 2em;
            font-size: .8em;
        }

        .table--box .row {
            display: -ms-flexbox;
            display: flex;
            display: -webkit-box;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            -webkit-box-flex: 1;
            -ms-flex: 1 1 auto;
            flex: 1 1 auto;
        }

        .table--box .col {
            width: 100%;
            padding: 12px;
            flex-basis: 0;
            -webkit-box-flex: 1;
            flex-grow: 1;
            max-width: 100%;
        }

        .table--box .col-1 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 4%;
            flex: 0 0 4%;
            max-width: 4%;
        }

        .table--box .col-3 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 31%;
            flex: 0 0 31%;
            max-width: 31%;
        }

        .table--box .col-4 {
            -webkit-box-flex: 0;
            -ms-flex: 0 0 35%;
            flex: 0 0 35%;
            max-width: 35%;
        }

        .table--box .row [class*=col-] {
            border-color: #000000;
            border-width: thin;
            border-style: solid;
        }

        .table--box .row [class*=col-]:not(:last-child) {
            border-right-width: 0;
        }

        .table--box .row:not(:last-child) [class*=col-] {
            border-bottom-width: 0;
        }

        .table--box .label {
            text-transform: uppercase;
            font-weight: 600;
            background-color: #e0e0e0;
        }

        .v-list {
            padding: 8px 0;
        }

        .v-list-item {
            align-items: center;
            display: flex;
            flex: 1 1 100%;
            letter-spacing: normal;
            outline: none;
            padding: 0 16px;
            position: relative;
            text-decoration: none;
            min-height: 40px;
        }

        .v-list--dense .v-list-item .v-list-item__content {
            padding: 8px 0;
        }

        .v-list-item__content {
            align-items: center;
            align-self: center;
            display: flex;
            flex-wrap: wrap;
            flex: 1 1;
            overflow: hidden;
            padding: 12px 0;
        }

        .pa-0 {
            padding: 0 !important;
        }

        .shrink {
            -webkit-box-flex: 0 !important;
            flex-grow: 0 !important;
            flex-shrink: 1 !important;
        }

        .grow {
            display: -ms-flexbox;
            display: flex;
            display: -webkit-box;
            flex-direction: column;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
        }

        .no-gutters > .col {
            padding: 0;
        }

        .header {
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: justify;
            -ms-flex-pack: justify;
            justify-content: space-between;
        }
    </style>
</head>
<body>
<div>

    <div class="header">
        <p>{{ __('Document generated') }}: {{ $date }}</p>
        <p>{{ $order->order_deadline->format('Y-m-d') }} | {{ $order->customer_service }}</p>
    </div>
    <div class="table">
        <div class="tbody">
            <div class="tr">
                <div class="td left">{{ __('Title') }}</div>
                <div class="td right">{{ $order->order_name }}</div>
            </div>
            <div class="tr">
                <div class="td left">{{ __('Order No.') }}</div>
                <div class="td right">{{ $order->order_number }}</div>
            </div>
            <div class="tr">
                <div class="td left">{{ __('Customer') }}</div>
                <div class="td right">{{ $order->customer->name }}</div>
            </div>
        </div>
    </div>

    @if($order->notes_supernumerary)
        <div class="table--box">
            <div class="row">
                <div class="col col-4 label">{{ __('Supernumerary books') }}</div>
                <div class="col col-">@nl2br($order->notes_supernumerary)</div>
            </div>
        </div>
    @endif

    @if($order->jobs_shipping->count() > 0)
        <div class="table--box">
            <div class="row label">
                <div class="col col-">{{ __('Logistic') }}</div>
            </div>
            <div class="row label">
                <div class="col col-4">{{ __('Name') }}</div>
                <div class="col col-4">{{ __('Quantity') }}</div>
                <div class="col col-">{{ __('Comments') }}</div>
            </div>
            @foreach($order->jobs_shipping as $shipping)
                <div class="row">
                    <div class="col col-4">{{ $shipping->name }}</div>
                    <div class="col col-4">{{ $shipping->quantity }}</div>
                    <div class="col col-">{{ $shipping->notes }}</div>
                </div>
            @endforeach
        </div>
    @endif

    @if($order->notes)
        <div class="table--box">
            <div class="row label">
                <div class="col col-">{{ __('Comments') }}</div>
            </div>
            <div class="row">
                <div class="col col-">@nl2br($order->notes)</div>
            </div>
        </div>
    @endif

    <div class="table--box">
        <div class="row label">
            <div class="col col-4">{{ __('One unit weight') }}</div>
            <div class="col col-3">{{ __('Total weight') }}</div>
            <div class="col col-">{{ __('Edition') }}</div>
        </div>
        <div class="row">
            <div class="col col-4">{{ number_format($order->mass_unit, 2) }} g</div>
            <div class="col col-3">{{ number_format($order->mass_total, 2) }} kg</div>
            <div class="col col-">{{ $order->qty_total }}</div>
        </div>
        @foreach($order->shipping_delivery as $index => $delivery)
            <div class="row" style="page-break-inside: avoid !important;">
                <div class="col col-1">
                    <span>{{ $delivery->delivery_number }}</span>
                </div>
                <div class="pa-0 col col-3">
                    <div class="v-list v-list--dense">
                        <div class="v-list-item">
                            <div class="v-list-item__content">{{ $delivery->delivery_method }}</div>
                        </div>
                        <div class="v-list-item">
                            <div class="v-list-item__content">{{ $delivery->supplier }}</div>
                        </div>
                        <div class="v-list-item">
                            <div class="v-list-item__content"><strong>{{ $delivery->name }}</strong></div>
                        </div>
                        <div class="v-list-item">
                            <div
                                class="v-list-item__content">{{ "$delivery->postal_code $delivery->city $delivery->street $delivery->building_number $delivery->suite_number" }}</div>
                        </div>
                        @if ($delivery->phone_number)
                            <div class="v-list-item">
                                <div class="v-list-item__content">{{ __('Phone') }} {{ $delivery->phone_number }}</div>
                            </div>
                        @endif
                        @if ($delivery->quantity)
                            <div class="v-list-item">
                                <div class="v-list-item__content">{{ __('Qty') }}
                                    : {{ $delivery->quantity }} {{ __('pcs') }}</div>
                            </div>
                        @endif
                        @if ($delivery->mass)
                            <div class="v-list-item">
                                <div class="v-list-item__content">{{ __('Mass') }}
                                    : {{ number_format($delivery->mass, 2) }} kg
                                </div>
                            </div>
                        @endif
                        @if ($delivery->main_surplus)
                            <div class="v-list-item" style="border-top: 1px solid #000;">
                                <div class="v-list-item__content" style="padding-bottom: 0;">
                                    <strong>{{ __('Recipient of the surplus') }}</strong></div>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="pa-0 col col-3">
                    <div class="v-list v-list--dense">
                        <div class="v-list-item">
                            <div class="v-list-item__content">{{ $delivery->quantity_setting }}</div>
                        </div>
                        <div class="v-list-item">
                            <div class="v-list-item__content">{{ $delivery->package->main }}</div>
                        </div>
                        @foreach($delivery->package->methods as $method)
                            <div class="v-list-item">
                                <div class="v-list-item__content">{{ $method }}</div>
                            </div>
                        @endforeach
                        @if (1 === 0)
                            @if (count($delivery->package->services)  > 0)
                                <hr>
                                @foreach($delivery->package->services as $service)
                                    <div class="v-list-item">
                                        <div class="v-list-item__content">
                                            <div>{{ $service->name }}</div>
                                            <div>{{ $service->quantity }} {{ __('pcs') }}</div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        @endif
                    </div>
                </div>
                <div class="pa-0 col col- grow">
                    <div class="label">
                        <div class="shrink col">{{ __('Comments') }}</div>
                    </div>
                    <div class="col" style="white-space: normal;">@nl2br($delivery->description)</div>
                    <div class="row shrink label">
                        <div class="col">
                            <div class="v-list-item">
                                <div class="v-list-item__content">{{ __('Signature') }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if($index + 1 === $order->shipping_delivery->count())
                <div class="page--break"></div>
            @endif
        @endforeach
    </div>
</div>

</body>
</html>
