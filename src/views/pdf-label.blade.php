<!DOCTYPE html>
<html lang="pl">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Label</title>
    <link href="https://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700&subset=latin-ext" rel="stylesheet">
    <style>
        * {
            overflow: visible !important;
        }
        html, body {
            background-color: #ffffff;
            color: #000000;
            font-family:  'PT Sans Narrow', sans-serif;
            font-stretch: condensed;
            font-weight: 400;
            font-size: 1em;
            margin: 0;
            padding: 3mm;
        }
        .table {
            width: 100%;
            display: flex;
            display: -webkit-box;
            flex-direction: column;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
        }
        .tr {
            width: 100%;
            display: -webkit-box;
            display: flex;
        }
        .tr .td:only-child {
            width: 100%;
        }
        .td {
            text-align: center;
            padding: 2mm 0 2mm 0;
            width: 50%;
            border: 1px solid #e5e5e5;
        }
        .tr + .tr .td {
            border-top: 0;
        }
        .td + .td {
            border-left: 0;
        }
        h1, h2, h3 {
            margin: 0;
            padding: 0;
        }
        h1 {
            font-size: 3.5rem;
            font-weight: 700;
            text-align: center;
        }
        h3 {
            font-style: italic;
        }
        h2 {
            font-weight: 400;
            line-height: 1em;
        }
        h1 + img {
            margin-bottom: 1rem;
        }
        img {
            display: block;
            margin-right: auto;
            margin-left: auto;
        }
    </style>
</head>
<body>
    <h1>{{ $order->order_number }}</h1>
    <img src='data:image/png;base64,{{ DNS1D::getBarcodePNG($order->order_number, 'C39', 1.5, 40 ) }}' alt=""/>
    <div class="table">
        <div class="tr">
            <div class="td">
                <h3>{{ __('Customer') }}:</h3>
                <h2>{{ \Illuminate\Support\Str::limit($order->customer_name, 35) }}</h2>
            </div>
        </div>
        <div class="tr">
            <div class="td">
                <h3>{{ __('Order name') }}:</h3>
                <h2>{{ \Illuminate\Support\Str::limit($order->order_name, 75) }}</h2>
            </div>
        </div>
        <div class="tr">
            <div class="td">
                <h3>{{ __('Customer service') }}:</h3>
                <h2>{{ $order->customer_service_fullname }}</h2>
            </div>
            <div class="td">
                <img src='data:image/png;base64,{{ DNS2D::getBarcodePNG($url, 'QRCODE', 3.5, 3.5) }}' alt="" />
            </div>
        </div>
    </div>
</body>
</html>
