Sam Job Ticket
================

This package creates job ticket to PROFIS. 

![Totem.com.pl](https://www.totem.com.pl/wp-content/uploads/2016/06/logo.png)

General System Requirements
-------------
- [PHP >7.4.0](http://php.net/)
- [Laravel ~6.*](https://github.com/laravel/framework)


Quick Installation
-------------
If necessary, use the composer to download the library

```
$ composer require rudashi/job-ticket
```

Remember to put repository in a composer.json

```
"repositories": [
    {
        "type": "vcs",
        "url":  "https://bitbucket.org/rudashi/jobticket.git"
    }
],
```

Authors
-------------

* **Borys Żmuda** - Lead designer - [LinkedIn](https://www.linkedin.com/in/boryszmuda/), [Portfolio](https://rudashi.github.io/)
